git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init : Cria um novo repositório Git, também cria uma ramificação principal

git status : Exibe as condições do diretório de trabalho e da área de staging. Lista quais arquivos
foram preparados, despreparados e não foram monitorados

git add arquivo/diretório : Adiciona uma alteração no arquivo/diretório à área de staging
git add --all = git add . : Adiciona todas as alterações do diretório atual à área de staging

git commit -m “Primeiro commit” : Cria de imediato um commit com a mensagem "Primeiro commit"

-------------------------------------------------------

git log : Exibe os commits feitos no repositório atual em ordem cronológica inversa
git log arquivo : Exibe os commits feitos no arquivo especificado
git reflog : Exibe os logs de referência do repositório (atualizações das pontas das ramificações)

-------------------------------------------------------

git show : Exibe os conteúdos dos objetos Git (commits, tags, trees, ...) do repositório
git show <commit> : Exibe as mudanças feitas para aquele commit específico

-------------------------------------------------------

git diff : Exibe as diferenças entre a cópia local e o índice sincronizado
git diff <commit1> <commit2> : Exibe as  diferenças entre dois commits diferentes

-------------------------------------------------------

git reset --hard <commit> : move o HEAD e as refs da ramificação, para a confirmação especificada.
Então, o índice de staging e o diretório de trabalho são redefinidos para o commit especificado

-------------------------------------------------------

git branch : Lista todas as ramificações no repositório
git branch -r : 
git branch -a : Lista todas as ramificações remotas
git branch -d <branch_name> : Exclusão "segura" da ramificação especificada. Impede que você exclua 
a ramificação se existir mudanças não mescladas
git branch -D <branch_name> : Força a exclusão da ramificação especificada
git branch -m <nome_novo> : Renomeia a ramificação atual para <nome_novo>
git branch -m <nome_antigo> <nome_novo> : Renomeia a ramificação de <nome_antigo> para <nome_novo>

-------------------------------------------------------

git checkout <branch_name> : Alterna da ramificação atual para a de <branch_name>
git checkout -b <branch_name> : Cria e alterna para a nova ramificação <branch_name>

-------------------------------------------------------

git merge <branch_name> : Mescla a ramificação <branch_name> à ramificação atual. O branch atual vai ser 
atualizado para refletir a mesclagem, mas o branch alvo não vai sofrer alterações

-------------------------------------------------------

git clone : Seleciona um repositório existente e cria um clone ou cópia do repositório de destino
git pull : Usado para buscar e baixar conteúdo de repositórios remotos e fazer a atualização 
imediata ao repositório local para que os conteúdos sejam iguais
git push : Usado para enviar o conteúdo do repositório local para um repositório remoto.
Transfere commits do repositório local a um repositório remoto.

-------------------------------------------------------

git remote -v : Lista as conexões remotas com outros repositórios, incluindo a URL de cada conexão
git remote add origin <url> : Cria uma nova conexão com um repositório remoto
git remote <url> origin :  

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


